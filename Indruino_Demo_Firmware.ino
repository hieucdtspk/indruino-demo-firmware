/*
  A simple (and/or stupid) HTTP RESTful server for Indruino

  HieuNT
  17/7/2017
*/

// Credit and based on below sample code:

/*
  Web Server

 A simple web server that shows the value of the analog input pins.
 using an Arduino Wiznet Ethernet shield.

 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 * Analog inputs attached to pins A0 through A5 (optional)

 created 18 Dec 2009
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe
 modified 02 Sept 2015
 by Arturo Guadalupi

 */

#include <SPI.h>
#include <Ethernet.h>

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 2, 10);

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("Hello HieuNT\n");
  // start the Ethernet connection and the server:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // initialize the Ethernet device not using DHCP:
    Ethernet.begin(mac, ip);
  }
  // print your local IP address:
  Serial.print("My IP address: ");
  ip = Ethernet.localIP();
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    Serial.print(ip[thisByte], DEC);
    Serial.print(".");
  }
  Serial.println();
  
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}

// Change actual sp and pv right here!
// int sp = 0, pv = 0;
float sp = 0, pv = 0;
char buffer[64];

void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new request");
    boolean currentLineIsBlank = true;

    while (client.connected()) {

      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        if (c == '\n' && currentLineIsBlank) {
          client.println("HTTP/1.1 200 OK");
          client.println("X-Powered-By: HieuNT Indruino RESTful server");
          client.println("Access-Control-Allow-Origin: *");
          client.println("Content-Type: application/json; charset=utf-8");
          // client.println("Content-Type: application/json");
          client.print("Content-Length: "); client.println(strlen(buffer), DEC);
          // client.println("ETag: W/"f-+W5qVbsf6wk7n/rWj1BeDxUN3NE"");
          // client.println("Date: Mon, 17 Jul 2017 07:03:02 GMT");
          // client.println("Connection: keep-alive");
          client.println("Connection: close");
          client.println();

          // HieuNT NOTE: change actual SP and PV right here!!!
          // sprintf(buffer, "{\"sp\":%d,\"pv\":%d}\r\n", sp++, pv++);
          sprintf(buffer, "{\"sp\":%f,\"pv\":%f}\r\n", sp, pv);
          sp += 0.1;
          pv += 0.1;

          client.print(buffer);
        }
        if (c == '\n') {
          currentLineIsBlank = true;
        } else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    delay(1);
    client.stop();
    Serial.println("request end");
  }
}
